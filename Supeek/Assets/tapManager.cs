﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tapManager : MonoBehaviour
{
    //int score;
    Vector2 tapPosition;
    public GameObject gameManagerObj;
    public GameObject animal;
    public Sprite upMove, downMove, leftMove, rightMove;
    SpriteRenderer spRend;

    private void Start()
    {
        spRend = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        
        
        tapPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Vector2.Distance(transform.position, animal.transform.position) < 0.5)
        {
            gameManager.score++;
            gameManagerObj.GetComponent<gameManager>().updateScoreText();
            animal.GetComponent<animalHide>().resetHide();
        }

        spriteManager();
    }

    private void LateUpdate()
    {
        transform.position = Vector2.MoveTowards(transform.position, tapPosition, 5f * Time.deltaTime);
    }

    void spriteManager()
    {
        if (tapPosition.y > transform.position.y)
            spRend.sprite = upMove;
        else if (tapPosition.y < transform.position.y)
            spRend.sprite = downMove;
        else if (tapPosition.x > transform.position.x)
            spRend.sprite = rightMove;
        else
            spRend.sprite = leftMove;

    }
}
